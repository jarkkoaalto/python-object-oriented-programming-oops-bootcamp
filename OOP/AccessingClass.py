class Student:
    def __init__(self,name,roll,marks):
        self.name = name
        self.roll = roll
        self.marks = marks

    def display(self):
        print("Student Name: ",self.name)
        print("Student Roll no: ",self.roll)
        print("Student Marks: ", self.marks)

    def profile(self):
        self.city = 'London'
        states = 'ABC'

S = Student('aws',1010,55.00)

print(S.__dict__) # symbol table __dict__

S.marks = 33.32
print(S.__dict__)

S.grade = 'A'
print(S.__dict__)
S.profile()
print(S.__dict__)
