class Demo:

    # DECORATOES
    def __init__(self): 
        pass

    # Instance method
    def A(self):
        pass

    @classmethod
    # Class method
    def B(cls):
        pass
    
    @staticmethod
    # Static method
    def C():
        pass

    @staticmethod
    ##  EXAMPLE
    def add(x,y):
        print("Sum: ", x+y)

Demo.add(10,30)

# If we use @staticmethod no error 
D = Demo()
D.add(12,23)

