class A:
    def displayA(self):
        print("Method-A")

class B(A):
    def displayB(self):
        print("Method-B")

class C(A):
    def displayC(self):
        print("Method-C")

b = B()
b.displayA()
b.displayB()

c =C()
c.displayA()
c.displayC()