class Student:
    def __init__(self):
        self.name = ''
        self.roll = 0
        self.marks = 0

    def getStudentData(self, name, roll, marks):
        self.name  = name
        self.roll = roll
        self.marks = marks
    
    def displayS(self):
        print(self.name, self.roll, self.marks)

class Placements(Student):
    def __init__(self):
        self.company = ""
        self.package = ""

    def getPlacementData(self, company, package):
        self.company = company
        self.package = package

    def displayP(self):
        print(self.company, self.package)

P = Placements()
P.getStudentData("John",1000,78.65)
P. getPlacementData('Dell',40055)
P.displayS()
P.displayP()