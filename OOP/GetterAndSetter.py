class Student:

    def setName(self,n):
        self.__name = n

    def getName(self):
        return self.__name

    def setMarks(self,m):
        self.__marks = m
    def getMarks(self):
        return self.__marks

    def display(self):
       # print("Name:", self.getName())
       # print("Marks:", self.getMarks())
        print('Name: ', self.__name)
        print('Marks: ', self.__marks)

S = Student()
S.setName("Kookos")
S.setMarks(12.22)
S.display()

S.display()