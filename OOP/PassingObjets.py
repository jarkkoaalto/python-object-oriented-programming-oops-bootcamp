class Student:
    def __init__(self,name,roll,marks):
        self.name = name
        self.roll = roll
        self.marks = marks

    def display(self):
        print("Student Name: ", self.name)
        print("Roll Number: ", self.roll)
        print("Marks:" , self.marks)

class Placements:
    def __init__(self, company, package, stud):
        self.company = company
        self.package = package
        self.stud = stud

    def display(self):
        self.stud.display()
        print("Company Name:",self.company)
        print("Package: ", self.package)

S = Student('Heli',1,44.55)
# S.display()

P = Placements('Acer', 40000, S)
P.display()