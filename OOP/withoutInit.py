class Student:

    def init(self, name, roll, marks):
        self.name = name
        self.roll = roll
        self.marks = marks

    def display(self):
        print("Student Name: ", self.name)
        print("Roll Number: ", self.roll)
        print("Marks: ", self.marks)



S = Student()
S.name = 'John'
S.roll = 101
S.marks = 67.34
S.display()

S.init("Johan",102,45.35)
S.display()