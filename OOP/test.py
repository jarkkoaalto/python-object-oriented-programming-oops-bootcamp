class Student:
    '''This is version 1.0'''

    def __init__(self, name, roll, marks):
        self.name = name
        self.roll = roll
        self.marks = marks 

    def  __str__(self):
        return 'This is Student Class'

    def display(self):
        print("Student Name: ", self.name)
        print("Roll Number: ", self.roll)
        print("Marks: ", self.marks)



S = [Student('aaa',101,50.34),
     Student('bbb',102, 45.04),
     Student('ccc',103, 87.22)]



for i in S:
    i.display()
            

# S = Student('noob1',101,78.75)
# S1 = Student('noob2',102,54.03)
# S2 = Student('noob3',103, 78.22)

# print(S.__doc__)
# print(S)

# S.display()
# S1.display()
# S2.display()