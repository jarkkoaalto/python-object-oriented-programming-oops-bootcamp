class A:
    def display(self):
        print("Method-A")

class B:
    def display(self):
        print("Method-B")
'''
class C(A,B): # Method A printed
    def displayC(self):
        print('Method-C')
'''
class C(B,A): # Method B printed, because only one method can be visible
    def displayC(self):
        print('Method-C')

c = C()
c.display()