class Collage:
    def __init__(self):
        print("Outer Class Constructor")


    class Student: # Not access inner class
        def __init__(self):
            print('Inner Class Constructor')
        def displayS(self):
            print("Student Method")

    def displayC(self):
        print("Collage Method")

C = Collage()
C.displayC()

S = C.Student()
S.displayS()