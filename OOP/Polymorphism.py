
# Polymorphism. Only latest method is available.  This time third is available and you have 
# put 2 parameters. And two upper method is discarded.
class Demo():
    def display(self):
        print('Value: ',0)
    
    def display(self,X):
        print("value: ",X)
    
    def display(self,x,y):
        print('Value: ',x,y)

D = Demo()
D.display(10,4)
# D.display() # Type error


class A:
    def display(self):
        print("value:",0)

class B(A): 
    def display(self,x):
        print("value: ",x)

b = B()
b.display(10)